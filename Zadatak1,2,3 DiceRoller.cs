using System;
using System.Collections.Generic;
using System.Text;

namespace LV2_RPPOON
{
    class DiceRoller
    {

        private List<PlayingDie> dice;
        private List<int> resultForEachRoll;
        public DiceRoller()
        {
            this.dice = new List<PlayingDie>();
            this.resultForEachRoll = new List<int>();
        }
        public void InsertDie(PlayingDie die)
        {
            dice.Add(die);
        }
        public void RemoveDie(PlayingDie die)
        {
            dice.Remove(die);
        }

        public void RollAllDice()
        {
            this.resultForEachRoll.Clear();
            foreach (PlayingDie die in dice)
            {
                this.resultForEachRoll.Add(die.Roll());
            }
        }
       
        public IList<int> GetRollingResults()
        {
            return new System.Collections.ObjectModel.ReadOnlyCollection<int>(
           this.resultForEachRoll
           );
        }
        public int DiceCount
        {
            get { return dice.Count; }
        }
    }
}
