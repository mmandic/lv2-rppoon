using System;
using System.Collections.Generic;
using System.Text;

namespace LV2_RPPOON
{
    class PlayingDie
    {
        private int numberOfSides;
        //private Random randomGenerator;//Zadatak1 i 2
        private RandomGenerator randomGenerator;

        //public PlayingDie(int numberOfSides)
        //{
        //    if (numberOfSides <= 6 && numberOfSides >= 1) this.numberOfSides = numberOfSides;
        //    this.randomGenerator = new Random();
        //} //Zadatak1

        //public PlayingDie(int numberOfSides, Random randomGenerator)
        //{
        //    if(numberOfSides <=6 && numberOfSides >=1 ) this.numberOfSides = numberOfSides;
        //    this.randomGenerator = new Random();
        //} //Zadatak2
        public PlayingDie(int numberOfSides)
        {
            if (numberOfSides <= 6 && numberOfSides >= 1) this.numberOfSides = numberOfSides;
            this.randomGenerator = RandomGenerator.GetInstance();
        }

        //public int Roll()
        //{
        //    int rolledNumber = randomGenerator.Next(1, numberOfSides + 1);
        //    return rolledNumber;
        //} //Zadatak1 i 2
        public int Roll()
        {
            int rolledNumber = randomGenerator.NextInt(1, numberOfSides + 1);
            return rolledNumber;

        }
    }
}
