using System;
using System.Collections.Generic;

namespace LV2_RPPOON
{
    class Program
    {
        static void Main(string[] args)
        {
            DiceRoller diceRoller = new DiceRoller();
            Random randomGenerator = new Random(); //Zadatak2 i 3
            const int numberOfDice = 20;

            //for (int i = 0; i < numberOfDice; i++) 
            //{ 
            //    diceRoller.InsertDie(new PlayingDie(6)); 
            //} //Zadatak1
            
            for (int i = 0; i < numberOfDice; i++) 
            {
                diceRoller.InsertDie(new PlayingDie(6)); 
            }
            diceRoller.RollAllDice();
            IList<int> results = diceRoller.GetRollingResults();
            foreach (int result in results)
            {
                Console.WriteLine(result);
            }
        }
    }
}
