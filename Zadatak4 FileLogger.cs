using System;
using System.Collections.Generic;
using System.Text;

namespace LV2_RPPOON
{
    class FileLogger : ILogger
    {
        private string filePath;

        public FileLogger(string filePath) { this.filePath = filePath; }

        public void Log(string message) 
        {
            using (System.IO.StreamWriter fileWriter = new System.IO.StreamWriter(this.filePath, true)) 
            { 
                fileWriter.WriteLine(message); 
            }
        }
    }
}
