using System;
using System.Collections.Generic;
using System.Text;

namespace LV2_RPPOON
{
    class ConsoleLogger
    {
        public void Log(ILogable data)
        {
            Console.WriteLine(data.GetStringRepresentation());
        }
    }
}
