using System;
using System.Collections.Generic;
using System.Text;

namespace LV2_RPPOON
{
    class DiceRoller:ILogable
    {

        private List<PlayingDie> dice;
        private List<int> resultForEachRoll;
        private Logger logger;
        public DiceRoller()
        {
            this.dice = new List<PlayingDie>();
            this.resultForEachRoll = new List<int>();
            this.logger = new Logger("Console", null);

        }
        public void InsertDie(PlayingDie die)
        {
            dice.Add(die);
        }
        public virtual void RemoveAllDice() { 
            this.dice.Clear(); 
            this.resultForEachRoll.Clear(); 
        }

        public void RollAllDice()
        {
            this.resultForEachRoll.Clear();
            foreach (PlayingDie die in dice)
            {
                this.resultForEachRoll.Add(die.Roll());
            }
        }
       
        public IList<int> GetRollingResults()
        {
            return new System.Collections.ObjectModel.ReadOnlyCollection<int>(
           this.resultForEachRoll
           );
        }
        public int DiceCount
        {
            get { return dice.Count; }
        }

        public string GetStringRepresentation() 
        { 
            StringBuilder stringBuilder = new StringBuilder();
            foreach (int result in this.resultForEachRoll) 
            { 
                stringBuilder.Append(result.ToString()).Append("\n");
            } 
            return stringBuilder.ToString(); 
        }
    }
}
